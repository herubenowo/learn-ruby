friends = ["Kevin", "Karen", "Oscar", "Angela", "Andy"]

for element in friends do
    puts element
end

puts "==========="

friends.each do |element|
    puts element
end

puts "==========="

for value in 0..5 do
    puts value
end

puts '==========='

6.times do |index|
    puts index
end