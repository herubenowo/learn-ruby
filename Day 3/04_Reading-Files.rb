File.open("files.txt", "r") do |file|
    # this will print all the data in the file
    # puts file.read()

    # puts "==========="
    # this will only print file per line
    # puts file.readline()
    # puts file.readline()

    # puts "==========="
    # this will only print file per character
    # puts file.readchar()
    # puts file.readchar()
    # puts file.readchar()
    # puts file.readchar()
    # puts file.readchar()

    # this will read a file and store it in an array
    # puts file.readlines()[2]

    for line in file.readlines() do
        puts line
    end
end

file = File.open("files.txt", "r")

puts file.readlines()[2]

file.close()