# a stands for append, u can write but only in the end of the file (in the end of the last character)
# w stands for overwriting all the file
# r+ stands for read and write at the beginning of the file
File.open("files.txt", "a") do |file|
    # it will keep modifying the file whenever we run the program
    file.write("\nOscar, Accounting")
end

# if there isnt any file with this name, it will automatically create a new file
File.open("index.html", "w") do |file|
    file.write("<h1>Hello</h1>")
end

File.open("files.txt", "r+") do |file|
    puts file.readline()
    # it will overwrite the 2nd line
    file.write("Overridens ")
end