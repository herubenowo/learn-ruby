puts 12 % 5
puts 2 ** 3

num = -22

puts "my fav num " + num.to_s # we have to convert number to string
puts num.abs() # give absolute number
puts "========"

num = 22.479

puts num.round() # rounding up or down depends on the decimal
puts num.ceil() # always rounding up a decimal number
puts num.floor() # always rounding down a decimal number
puts "========="

puts Math.sqrt(49) #will give us a square root
puts Math.log(num) #logarithm
puts "======"

puts 10 / 7 # will just return the integer (not decimal or float)
puts 10 / 7.0 # we have to assign one of the number to be a decimal or float