# everything you input with ruby will be a string
puts "Enter your number: "
num1 = gets.chomp()
puts "Enter another number: "
num2 = gets.chomp()

puts "What do you want to do with these numbers?\n1.Addition\n2.Multiplication\n3.Division"
selection = gets.chomp().to_i
if selection == 1
    puts "Your result is: "
    puts (num1.to_i + num2.to_i) # the input need to be converted to integer, if the input want to be a decimal, use to_f (float)  
elsif selection == 2
    puts "Your result is: "
    puts (num1.to_i * num2.to_i) # the input need to be converted to integer, if the input want to be a decimal, use to_f (float)
elsif selection == 3
    puts "Your result is: "
    puts (num1.to_i / num2.to_i) # the input need to be converted to integer, if the input want to be a decimal, use to_f (float)
else
    puts "Please input selection 1, 2, or 3!"
end