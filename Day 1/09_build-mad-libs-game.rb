puts "Enter a color: "
color = gets.chomp().to_s
puts "Enter a plural noun: "
plural_noun = gets.chomp().to_s
puts "Enter a celebrity: "
celebrity = gets.chomp().to_s

puts "============="
puts "Roses are " + color
puts plural_noun + " are blue"
puts "I love " + celebrity