friends = Array["Kevin", "Karen", "Oscar", "Andy"]

puts friends
puts friends[-2] # reverse from the last index and it's value is -1
puts friends[0, 2]

friends[0] = "Dwight"
puts friends[0]

tools = Array.new

tools[0] = "Knife"
tools[4] = "Gas Stove"

puts tools
puts tools.include? "Knife"
puts tools.reverse()
puts friends.sort() #sorting depends on the data types, if there's any different in datatypes inside the array it will return an error